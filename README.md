# Clock_Drift_Analysis



## Test configuration

- Analog Discovery
- PWM generation
- Different clock configurations

## Results

## Conclusions

- HSE provides excellent performance (100 ppm)
- HSI is less accurate (2000 to 10000 ppm), almost 1%!
- MSI is a bit worse than HSI
- Only STM32L4 can use MSI with LSE calibration to improve performance (550 ppm)
- On other MCUs, LSE has no impact when using HSI
- HSI is heavily impacted by temperature
- Frenquency has little impact on drift

## To-do

- RP2040
- ESP32 modules

